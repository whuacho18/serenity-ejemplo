## Usar Maven

Abrir una ventana de comando de window y ejecutar:

    mvn clean verify

## Ver los reportes

Ambos comandos proporcionados anteriormente producirán un reporte de prueba de Serenity en el directorio `target/site/serenity`.

