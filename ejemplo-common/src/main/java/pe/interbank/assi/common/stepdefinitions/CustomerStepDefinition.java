package pe.interbank.assi.common.stepdefinitions;

import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import cucumber.api.java.es.Y;
import net.thucydides.core.annotations.Steps;
import pe.interbank.assi.common.steps.CustomerSteps;

public class CustomerStepDefinition {
    public static String number = "";

    @Steps
    CustomerSteps customerSteps;

    @Dado("^que la persona es un (CLIENTE|POTENCIAL) con DNI (.*)$")
    public void findPersonType(String personType, String documentNumber) {
        number = documentNumber;
        customerSteps.getPersonType(documentNumber);
        customerSteps.shouldHaveThePersonType(personType);
    }

    @Y("^el cliente no debería tener ley de protección de datos$")
    public void customerShouldNotHaveLdpd() {
        customerSteps.getStatusLdpd();
        customerSteps.shouldNotHaveLdpdAffiliation();
    }

    @Y("^el cliente no debería estar afiliado a la banca SMS$")
    public void customerShouldNotHaveSmsBanking() {
        customerSteps.getStatusSmsBanking();
        customerSteps.shouldNotHaveSmsBanking();
    }

    @Y("^el cliente debería tener ley de protección de datos$")
    public void customerShouldHaveLdpd() {
        customerSteps.getStatusLdpd();
        customerSteps.shouldHaveLdpdAffiliation();
    }

    @Y("^el cliente debería estar afiliado a la banca SMS$")
    public void customerShouldHaveSmsBanking() {
        customerSteps.getStatusSmsBanking();
        customerSteps.shouldHaveSmsBanking();
    }

    @Entonces("^el cliente debería tener una cuenta creada$")
    public void customerShouldHaveAccountCreated() {
        customerSteps.findAccounts(number);
        customerSteps.shouldHaveAccountCreated();
    }
}