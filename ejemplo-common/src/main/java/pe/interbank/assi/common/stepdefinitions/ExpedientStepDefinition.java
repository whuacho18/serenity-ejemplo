package pe.interbank.assi.common.stepdefinitions;

import cucumber.api.java.es.Y;
import net.thucydides.core.annotations.Steps;
import pe.interbank.assi.common.steps.ExpedientSteps;

public class ExpedientStepDefinition {
    @Steps
    ExpedientSteps expedientSteps;

    @Y("^el número de expediente debería estar cerrado$")
    public void expedientShouldHaveStatusClosed() {
        expedientSteps.findStatusExpedient();
        expedientSteps.shouldHaveClosedStatus();
    }
}
