﻿package pe.interbank.assi.common.steps;

import com.github.javafaker.Faker;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.hamcrest.Matchers;
import pe.interbank.assi.common.utils.PropertyManager;
import pe.interbank.assi.common.utils.Security;
import pe.interbank.assi.common.utils.Util;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class CustomerSteps {
    private static String TEMPLATE_CUSTOMER_CREATION = "templates/crearCliente.json";
    private String personTypeExpected;
    private String flagLdpdExpected;
    private String flagSmsBankingExpected;
    private List<String> accounts;

    @Step("Crea el cliente con el número de documento: {0}")
    public void createCustomer(String documentNumber) {
        Faker faker = new Faker();
        String body = Util.getTemplate(TEMPLATE_CUSTOMER_CREATION)
                .replace("{{viDocumentNumber}}", documentNumber)
                .replace("{{viFirstName}}", faker.address().firstName())
                .replace("{{viSecondName}}", faker.address().firstName())
                .replace("{{viLastName}}", faker.address().lastName())
                .replace("{{viMotherName}}", faker.address().lastName());

        String customerId = SerenityRest.given().header("Authorization", Security.createAccessToken())
                .contentType(ContentType.JSON)
                .header("Ocp-Apim-Subscription-Key", PropertyManager.getInstance().getCustomerSubscriptionKey())
                .headers(Util.getHeadersWithUserCode())
                .body(body)
                .post( PropertyManager.getInstance().getBaseUrl() + "")
                .then().assertThat().statusCode(Util.STATUS_CODE_CREATED)
                .extract().response().jsonPath().getString("customer.id");
    }

    @Step("Consulta el tipo de persona con el tipo de documento: DNI y número de documento: {0}")
    public void getPersonType(String documentNumber) {
        Response response =  SerenityRest.given().header("Authorization", Security.createAccessToken())
                .header("Ocp-Apim-Subscription-Key", PropertyManager.getInstance().getCustomerSubscriptionKey())
                .headers(Util.getHeadersWithUserCode())
                .param("identityDocumentType", "")
                .param("identityDocumentNumber", documentNumber)
                .get(PropertyManager.getInstance().getBaseUrl()+ "");
        System.out.println("[CONSULTA CLIENTE] Response: " + response.getBody().asString());

        if (response.getStatusCode() == 404) {
            personTypeExpected = "NO_CLIENTE";
        } else {
            personTypeExpected =  response.then().extract().response().jsonPath().getString("customer.personType");
        }
    }

    @Step("El tipo de persona debería ser: {0}")
    public void shouldHaveThePersonType(String personType) {
        assertThat(personType, containsString(personTypeExpected));
    }

    @Step("Consulta ley de protección de datos")
    public void getStatusLdpd() {
        Response response =  SerenityRest.given().header("Authorization", Security.createAccessToken())
                .header("Ocp-Apim-Subscription-Key", PropertyManager.getInstance().getAssiSubscriptionKey())
                .headers(Util.getCommonHeaders())
                .param("personType", "")
                .param("identityDocumentType", "")
                .param("identityDocumentNumber", "")
                .get(PropertyManager.getInstance().getBaseUrl()+ "");
        flagLdpdExpected = response.then().extract().response().jsonPath().getString("flagLPD");
    }

    @Step("El estado del LDPD debería ser no afiliado")
    public void shouldNotHaveLdpdAffiliation() {
        assertThat(flagLdpdExpected, nullValue());
    }

    @Step("El estado del LDPD debería ser afiliado")
    public void shouldHaveLdpdAffiliation() {
        assertThat(flagLdpdExpected, equalToIgnoringCase(""));
    }

    @Step("Consulta afiliación banca SMS")
    public void getStatusSmsBanking() {
        Response response =  SerenityRest.given().header("Authorization", Security.createAccessToken())
                .header("Ocp-Apim-Subscription-Key", PropertyManager.getInstance().getAssiSubscriptionKey())
                .headers(Util.getHeadersWithUserCode())
                .param("customerId", "")
                .get(PropertyManager.getInstance().getBaseUrl()+ "");

        if (response.getStatusCode() == 404) {
            flagSmsBankingExpected = "No Afiliado";
        } else {
            flagSmsBankingExpected = response.then().extract().response().jsonPath().getString("affiliationStatus");
        }
    }

    @Step("El estado de la banca SMS es no afiliado")
    public void shouldNotHaveSmsBanking() {
        assertThat(flagSmsBankingExpected, equalToIgnoringCase(""));
    }

    @Step("El estado de la banca SMS es afiliado")
    public void shouldHaveSmsBanking() {
        assertThat(flagSmsBankingExpected, equalToIgnoringCase(""));
    }

    @Step("Consulta las cuentas del cliente")
    public void findAccounts(String documentNumber) {
        Response response = SerenityRest.given().header("Authorization", Security.createAccessToken())
                .header("Ocp-Apim-Subscription-Key", PropertyManager.getInstance().getProductsSubscriptionKey())
                .headers(Util.getHeadersWithUserCode())
                .get(PropertyManager.getInstance().getBaseUrl() + ""
                        + documentNumber+"");
        System.out.println("[] Response: " + response.getBody().asString());
        if (response.getStatusCode() == 200) {
            accounts = response.then().extract().jsonPath().getList("accounts.subType");
        }
        if (response.getStatusCode() == 404) {
            response.then().assertThat().body("userMessage", Matchers.equalTo(""));
        }
    }

    @Step("El cliente debería tener la cuenta creada")
    public void shouldHaveAccountCreated() {
        assertThat(accounts, notNullValue());
    }
}
