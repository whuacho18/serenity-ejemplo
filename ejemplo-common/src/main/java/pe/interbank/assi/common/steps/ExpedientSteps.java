package pe.interbank.assi.common.steps;

import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import pe.interbank.assi.common.utils.PropertyManager;
import pe.interbank.assi.common.utils.Security;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class ExpedientSteps {
    private String status;

    @Step("Consulta el estado del expediente")
    public void findStatusExpedient() {
        Response response =  SerenityRest.given().header("Authorization", Security.createAccessToken())
                .header("Ocp-Apim-Subscription-Key", PropertyManager.getInstance().getAssiCommonSubscriptionKey())
                .pathParam("expedientNumber", "")
                .get(PropertyManager.getInstance().getBaseUrl()+ "");
        status = response.then().extract().response().jsonPath().getString("status");
    }

    @Step("El estado del expediente es CREADO")
    public void shouldHaveCreatedStatus() {
        assertThat(status, equalTo(""));
    }

    @Step("El estado del expediente es CERRADO")
    public void shouldHaveClosedStatus() {
        assertThat(status, equalTo(""));
    }
}
