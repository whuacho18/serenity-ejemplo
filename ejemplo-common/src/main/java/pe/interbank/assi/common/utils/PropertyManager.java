package pe.interbank.assi.common.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyManager {

    private static PropertyManager propertyManager;
    private static final Object lock = new Object();
    private static final String APPLICATION_PROPERTIES = "";


    public static PropertyManager getInstance() {
        if (propertyManager == null) {
            synchronized (lock) {
                propertyManager = new PropertyManager();
                propertyManager.loadData();
            }
        }
        return propertyManager;
    }

    private void loadData() {
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream(System.getProperty("user.home") + File.separator + APPLICATION_PROPERTIES));
        } catch (IOException e) {
        }


    }



}