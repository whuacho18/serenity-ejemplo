package pe.interbank.assi.common.utils;

import io.restassured.http.ContentType;

import static io.restassured.RestAssured.given;

public class Security {
    public static String createAccessToken() {
        String body = "";
        try {
            return "Bearer " + given().header("", "")
                    .contentType(ContentType.JSON)
                    .header("", PropertyManager.getInstance().getSecuritySubscriptionKey())
                    .body(body)
                    .post(PropertyManager.getInstance().getBaseUrl() + "")
                    .then()
                    .extract().response().jsonPath().getString("access_token");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
