package pe.interbank.assi.common.utils;

import org.apache.commons.io.IOUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class Util {

    public static final int STATUS_CODE_UNAUTHORIZED = 401;
    public static final int STATUS_CODE_NOT_FOUND = 404;
    public static final int STATUS_CODE_OK = 200;
    public static final int STATUS_CODE_CREATED = 201;
    public static final int STATUS_CODE_FORBIDDEN = 403;
    public static final int STATUS_CODE_SERVICE_UNAVAILABLE = 503;
    public static final int STATUS_CODE_BAD_REQUEST = 400;
    public static final int STATUS_CODE_PRECONDITION_FAILED = 412;
    public static final int STATUS_CODE_CONFLICT = 409;

    private static Map<String, Object> headerMap = new HashMap<String, Object>();

    public static WebElement expandRootElement(WebElement element) {
        return (WebElement) ((JavascriptExecutor) getDriver()).executeScript("return arguments[0].shadowRoot", element);
    }

    public static void writeEachCharacter(WebElement webElement, String[] caracteres) {
        for (int i = 0; i < caracteres.length; i++) {
            webElement.sendKeys(caracteres[i]);
        }
    }

    public static Map<String, Object> getCommonHeaders() {
        headerMap.put("application", "");
        headerMap.put("company", "");
        headerMap.put("storeCode", "");
        return headerMap;
    }

    public static Map<String, Object> getHeadersWithUserCode() {
        getCommonHeaders();
        headerMap.put("user", "");
        return headerMap;
    }

    public static String getTemplate(String templatePath) {
        try {
            return IOUtils.toString(new ClassPathResource(templatePath).getInputStream(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getDocumentNumber() {
        String randomNumber = String.valueOf(10 + new Random().nextInt(89));
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        String formatDate = dateFormat.format(new Date()).replaceAll(":", "");
 /*       String randomLetter = RandomStringUtils.randomAlphabetic(1).toUpperCase();
        if (customer.getIdentityDocumentType().equals("CE")) {
            return (randomNumber + formatDate + randomLetter);
        }*/
        return (randomNumber + formatDate);
    }

    public static void waitFor(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void scrollTopPage() {
        ((JavascriptExecutor)getDriver()).executeScript("window.scrollTo(0, 100)");
    }
}
