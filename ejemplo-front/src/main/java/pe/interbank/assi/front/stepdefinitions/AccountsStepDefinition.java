package pe.interbank.assi.front.stepdefinitions;

import cucumber.api.java.es.Cuando;
import net.thucydides.core.annotations.Steps;
import pe.interbank.assi.front.steps.*;

public class AccountsStepDefinition {
    @Steps
    LoginSteps loginSteps;
    @Steps
    SearchClientSteps searchClientSteps;
    @Steps
    DashboardSteps dashboardSteps;
    @Steps
    PersonalInfoSteps personalInfoSteps;
    @Steps
    AddressInfoSteps addressInfoSteps;
    @Steps
    ConfigurationSteps configurationSteps;

    @Cuando("^se realiza una apertura de cuenta (.*) INDIVIDUAL de CLIENTE$")
    public void registerCustomerAccount(String subproduct) {
        loginSteps.enter();
        loginSteps.enterCredentials();
        searchClientSteps.searchClient();
        dashboardSteps.enterChannel();
        dashboardSteps.selectProduct(subproduct);
        personalInfoSteps.enterPersonalInformationCustomer();
        configurationSteps.enterConfigurationAccountInformation();
    }

}
