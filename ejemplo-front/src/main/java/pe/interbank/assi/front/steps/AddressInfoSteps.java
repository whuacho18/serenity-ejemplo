﻿package pe.interbank.assi.front.steps;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import pe.interbank.assi.front.ui.AddressInfoPage;

public class AddressInfoSteps {
    @Steps
    AddressInfoPage addressInfoPage;

    @Step("Ingresa los datos de dirección")
    public void enterAddressInformation() {
        addressInfoPage.setDepartment("");
        addressInfoPage.setProvince("");
        addressInfoPage.setDistrict("");
        addressInfoPage.setStreetType("");
        addressInfoPage.setStreetName("");
        addressInfoPage.setStreetNumber("");
        addressInfoPage.setBlock("");
        addressInfoPage.setLot("");
        addressInfoPage.setApartment("");

        addressInfoPage.setNeighborhood("");
        addressInfoPage.setLandMark("");

        addressInfoPage.clickButtonNext();
    }
}
