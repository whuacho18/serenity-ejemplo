﻿package pe.interbank.assi.front.steps;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import pe.interbank.assi.front.ui.ConfigurationAccountPage;

public class ConfigurationSteps {
    @Steps
    ConfigurationAccountPage configurationAccountPage;

    @Step("Ingresa la configuración de la cuenta")
    public void enterConfigurationAccountInformation() {
        configurationAccountPage.setCurrency("");
        Serenity.takeScreenshot();
        configurationAccountPage.clickAffidavit();
        Serenity.takeScreenshot();
        configurationAccountPage.clickButtonCreateAccount();
        Serenity.takeScreenshot();
    }
}
