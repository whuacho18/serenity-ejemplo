package pe.interbank.assi.front.steps;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import pe.interbank.assi.front.ui.DashboardPage;
import pe.interbank.assi.front.ui.modals.ChannelModalPage;

public class DashboardSteps {
    @Steps
    ChannelModalPage channelModalPage;
    @Steps
    DashboardPage dashboardPage;

    @Step("Ingresa el canal de venta")
    public void enterChannel() {
        channelModalPage.setChannel("");
        Serenity.takeScreenshot();
        channelModalPage.clickButtonSave();
    }

    @Step("Selecciona el producto: {0}")
    public void selectProduct(String subproduct) {
        dashboardPage.setSearch(subproduct);
        dashboardPage.clickOpenProductAvailable();
        dashboardPage.clickAccountType("");
        Serenity.takeScreenshot();
        dashboardPage.clickButtonOpen();
    }
}
