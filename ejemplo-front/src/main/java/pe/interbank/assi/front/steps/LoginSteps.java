﻿package pe.interbank.assi.front.steps;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import pe.interbank.assi.front.ui.LoginPage;

public class LoginSteps {
    @Steps
    LoginPage loginPage;

    @Step("Ingresa a la página")
    public void enter() {
        loginPage.setDefaultBaseUrl("");
        loginPage.open();
    }

    @Step("Ingresa las credenciales")
    public void enterCredentials() {
        loginPage.setUser("");
        loginPage.setPassword("");
        Serenity.takeScreenshot();
        loginPage.clickButtonLogin();
    }
}
