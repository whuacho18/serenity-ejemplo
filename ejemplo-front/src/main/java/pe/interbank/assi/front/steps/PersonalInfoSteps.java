package pe.interbank.assi.front.steps;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import pe.interbank.assi.front.ui.PersonalInfoPage;

public class PersonalInfoSteps {
    @Steps
    PersonalInfoPage personalInfoPage;

    @Step("Ingresa los datos personales del cliente")
    public void enterPersonalInformationCustomer() {
        personalInfoPage.setEmail("");
        personalInfoPage.setCarrier("");
        personalInfoPage.setPhoneNumber("");
        Serenity.takeScreenshot();
        personalInfoPage.clickButtonNext();
    }
}
