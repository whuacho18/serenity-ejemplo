﻿package pe.interbank.assi.front.steps;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import pe.interbank.assi.common.stepdefinitions.CustomerStepDefinition;
import pe.interbank.assi.front.ui.SearchClientPage;

public class SearchClientSteps {
    @Steps
    SearchClientPage searchClientPage;

    @Step("Ingresa el tipo y número de documento")
    public void searchClient() {
        searchClientPage.setDocumentType("");
        searchClientPage.setDocumentNumber(CustomerStepDefinition.number);
        Serenity.takeScreenshot();
        searchClientPage.clickButtonValidate();
    }
}
