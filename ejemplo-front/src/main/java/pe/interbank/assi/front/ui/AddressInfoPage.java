package pe.interbank.assi.front.ui;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import pe.interbank.assi.common.utils.Util;
import pe.interbank.assi.front.ui.components.Atoms;
import pe.interbank.assi.front.ui.components.Molecules;
import pe.interbank.assi.front.ui.components.Particles;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

/**
 * @author Walter Huacho
 */
public class AddressInfoPage {
    
    public void setDepartment(String department) {
        //ApiCommons.waitFor(1);
        if (isDepartmentDisplayed()) {
            Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#addressDepartment " + Molecules.IBK_AUTOCOMPLETEGROUP_TEXT_CPV)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_TEXTFIELD_AUTOCOMPLETE_CPV)))
                    .findElement(By.cssSelector("input")).sendKeys(department);
        }
    }

    private boolean isDepartmentDisplayed() {
        try {
            WebElement department = getDriver().findElement(By.cssSelector("#addressDepartment " + Molecules.IBK_AUTOCOMPLETEGROUP_TEXT_CPV));
            return department.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setProvince(String province) {
       // ApiCommons.waitFor(1);
        if (isProvinceDisplayed()) {
            Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#addressProvince " + Molecules.IBK_AUTOCOMPLETEGROUP_TEXT_CPV)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_TEXTFIELD_AUTOCOMPLETE_CPV)))
                    .findElement(By.cssSelector("input")).sendKeys(province);
        }
    }

    private boolean isProvinceDisplayed() {
        try {
            WebElement province = getDriver().findElement(By.cssSelector("#addressProvince " + Molecules.IBK_AUTOCOMPLETEGROUP_TEXT_CPV));
            return province.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setDistrict(String district) {
       // ApiCommons.waitFor(1);
        if (isDistrictDisplayed()) {
            Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#addressDistrict " + Molecules.IBK_AUTOCOMPLETEGROUP_TEXT_CPV)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_TEXTFIELD_AUTOCOMPLETE_CPV)))
                    .findElement(By.cssSelector("input")).sendKeys(district);
        }
    }

    private boolean isDistrictDisplayed() {
        try {
            WebElement district = getDriver().findElement(By.cssSelector("#addressDistrict " + Molecules.IBK_AUTOCOMPLETEGROUP_TEXT_CPV));
            return district.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setStreetType(String streetType) {
        if(isStreetTypeDisplayed()) {
            ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector(\"#streetType " +
                    "ibk-inputgroup-select\").shadowRoot.querySelector(\"div ibk-select\")" +
                    ".shadowRoot.querySelector(\"#" + streetType + "\").click()");
        }
    }

    private boolean isStreetTypeDisplayed() {
        try {
             WebElement streetType = getDriver().findElement(By.cssSelector("#streetType " + Molecules.IBK_INPUTGROUP_SELECT));
             return streetType.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setStreetName(String streetName) {
        if(isStreetNameDisplayed()) {
            Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#streetName " + Molecules.IBK_INPUTGROUP_TEXT)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                    .findElement(By.cssSelector("input")).sendKeys(streetName);
        }
    }

    private boolean isStreetNameDisplayed() {
        try {
            WebElement streetName = getDriver().findElement(By.cssSelector("#streetName " + Molecules.IBK_INPUTGROUP_TEXT));
            return streetName.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setStreetNumber(String streetNumber) {
        if (isStreetNumberDisplayed()) {
            Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#streetNumber " + Molecules.IBK_INPUTGROUP_TEXT)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                    .findElement(By.cssSelector("input")).sendKeys(streetNumber);
        }
    }

    private boolean isStreetNumberDisplayed() {
        try {
            WebElement streetNumber = getDriver().findElement(By.cssSelector("#streetNumber " + Molecules.IBK_INPUTGROUP_TEXT));
            return streetNumber.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setBlock(String block) {
        if (isBlockDisplayed()) {
            Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#streetBlock " + Molecules.IBK_INPUTGROUP_TEXT)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                    .findElement(By.cssSelector("input")).sendKeys(block);
        }
    }

    private boolean isBlockDisplayed() {
        try {
            WebElement block = getDriver().findElement(By.cssSelector("#streetBlock " + Molecules.IBK_INPUTGROUP_TEXT));
            return block.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setLot(String lot) {
        if (isLotDisplayed()) {
            Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#streetLot " + Molecules.IBK_INPUTGROUP_TEXT)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                    .findElement(By.cssSelector("input")).sendKeys(lot);
        }
    }

    private boolean isLotDisplayed() {
        try {
            WebElement lot = getDriver().findElement(By.cssSelector("#streetLot " + Molecules.IBK_INPUTGROUP_TEXT));
            return lot.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setApartment(String apartment) {
        if (isApartmentDisplayed()) {
            Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#streetInt " + Molecules.IBK_INPUTGROUP_TEXT)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                    .findElement(By.cssSelector("input")).sendKeys(apartment);
        }
    }

    private boolean isApartmentDisplayed() {
        try {
            WebElement apartment = getDriver().findElement(By.cssSelector("#streetInt " + Molecules.IBK_INPUTGROUP_TEXT));
            return apartment.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setNeighborhood(String neighborhood) {
        if (isNeighborhoodDisplayed()) {
            Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#addressUrbanization " + Molecules.IBK_INPUTGROUP_TEXT)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                    .findElement(By.cssSelector("input")).sendKeys(neighborhood);
        }
    }

    private boolean isNeighborhoodDisplayed() {
        try {
            WebElement neighborhood = getDriver().findElement(By.cssSelector("#addressUrbanization " + Molecules.IBK_INPUTGROUP_TEXT));
            return neighborhood.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setLandMark(String landMark) {
        if (isLandMarkDisplayed()) {
            Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#addressReference " + Molecules.IBK_INPUTGROUP_TEXT)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                    .findElement(By.cssSelector("input")).sendKeys(landMark);
        }
    }

    private boolean isLandMarkDisplayed() {
        try {
            WebElement landMark = getDriver()
                    .findElement(By.cssSelector("#addressReference " + Molecules.IBK_INPUTGROUP_TEXT));
            return landMark.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void clickButtonNext() {
      //  if (this.globalData.getData("viPersonType").equalsIgnoreCase("NO_CLIENTE")) {
            Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#row-buttons div:nth-child(2) " + Atoms.IBK_BUTTON)))
                    .findElement(By.cssSelector("button " + Particles.IBK_FONT)))
                    .findElement(By.cssSelector("p")).click();
      //  }
    }
}
