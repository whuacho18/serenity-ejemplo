package pe.interbank.assi.front.ui;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import pe.interbank.assi.common.utils.Util;
import pe.interbank.assi.front.ui.components.Atoms;
import pe.interbank.assi.front.ui.components.Molecules;
import pe.interbank.assi.front.ui.components.Particles;
import pe.interbank.assi.front.ui.modals.ErrorServiceModalPage;

import java.util.List;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

/**
 * @author Walter Huacho
 */
public class ConfigurationAccountPage extends PageObject {

    @Steps
    ErrorServiceModalPage errorServiceModalPage;

    public void setCurrency(String currency) {
        ((JavascriptExecutor)getDriver()).executeScript("window.scrollBy(0,-500)");
        if (currency.equalsIgnoreCase("PEN") || currency.equalsIgnoreCase("EURO")) {
            ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#currency ibk-inputgroup-tabs')" +
                    ".shadowRoot.querySelector('div div div ibk-tab').shadowRoot.querySelector('div div:nth-child(1) " +
                    "button div.font-container p').click()");
        }
        if(currency.equalsIgnoreCase("USD")) {
            ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#currency ibk-inputgroup-tabs')" +
                    ".shadowRoot.querySelector('div div div ibk-tab').shadowRoot.querySelector('div div:nth-child(2) " +
                    "button div.font-container p').click()");
        }
    }

    public void clickDataProtection() {
        if (isDataProtectionDisplayed()) {
            ((JavascriptExecutor) getDriver()).executeScript("window.scrollBy(0,1200)");
            Util.expandRootElement(getDriver().findElement(By.cssSelector("#lpdpCheckbox")))
                    .findElement(By.cssSelector("div label")).click();
        }
    }

    private boolean isDataProtectionDisplayed() {
        try {
            WebElement dataProtection = getDriver().findElement(By.cssSelector("#lpdpCheckbox"));
            return dataProtection.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void clickSmsBanking() {
        if (isSmsBankingDisplayed()) {
            Util.expandRootElement(getDriver().findElement(By.cssSelector("#smsCheckbox")))
                    .findElement(By.cssSelector("div label")).click();
        }
    }

    private boolean isSmsBankingDisplayed() {
        try {
            WebElement smsBanking = getDriver().findElement(By.cssSelector("#smsCheckbox"));
            return smsBanking.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void clickEmailNotification() {
        if (isEmailNotificationDisplayed()) {
            Util.expandRootElement(getDriver().findElement(By.cssSelector("#mailCheckbox")))
                    .findElement(By.cssSelector("div label")).click();
        }
    }

    private boolean isEmailNotificationDisplayed() {
        try {
            WebElement emailNotification = getDriver().findElement(By.cssSelector("#mailCheckbox"));
            return emailNotification.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void clickButtonCreateAccount() {
        int count = 1;
        Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("#create-account " + Atoms.IBK_BUTTON)))
                .findElement(By.cssSelector("button div div.text-container " + Particles.IBK_FONT)).click();

        do {
            Util.waitFor(count);
            count++;
            if (errorServiceModalPage.isUndestandButtonDisplayed()) {
                errorServiceModalPage.clickUnderstandButton();
                break;
            }
        } while (!isDisplayedToast());

        Util.waitFor(4);
    }

    private boolean isDisplayedToast() {
        try {
            WebElement toast = getDriver().findElement(By.cssSelector(Atoms.IBK_TOAST));
            return toast.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setAmount(String amount) {
        if (isAmountDisplayed()) {
            Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#amount " + Molecules.IBK_INPUTGROUP_AMOUNT)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_INPUT_AMOUNT)))
                    .findElement(By.cssSelector("div input")).sendKeys(amount);

            Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#amount " + Molecules.IBK_INPUTGROUP_AMOUNT)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_INPUT_AMOUNT)))
                    .findElement(By.cssSelector("div span")).click();
        }
       // ApiCommons.waitFor(4);
    }

    private boolean isAmountDisplayed() {
        try {
            WebElement amount = getDriver().findElement(By.cssSelector("#amount " + Molecules.IBK_INPUTGROUP_AMOUNT));
            return amount.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setTermDeposit() {
        if (isSimulatorDisplayed()) {
            selectTermDeposit();
        }
    }

    private boolean isSimulatorDisplayed() {
        try {
            WebElement simulatorBorder = getDriver().findElement(By.cssSelector("div.simulator-border div table tbody tr"));
            return simulatorBorder.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    private void selectTermDeposit() {
/*        switch (this.globalData.getData("viTermDeposit")) {
            case "DEPOSITO_31":
                getDriver().findElement(By.cssSelector("div.simulator-border div table tbody tr:nth-child(1)")).click();
                break;
            case "DEPOSITO_90":
                getDriver().findElement(By.cssSelector("div.simulator-border div table tbody tr:nth-child(2)")).click();
                break;
            case "DEPOSITO_180":
                getDriver().findElement(By.cssSelector("div.simulator-border div table tbody tr:nth-child(3)")).click();
                break;
            case "DEPOSITO_365":
                getDriver().findElement(By.cssSelector("div.simulator-border div table tbody tr:nth-child(4)")).click();
                break;
            case "DEPOSITO_720":
                getDriver().findElement(By.cssSelector("div.simulator-border div table tbody tr:nth-child(5)")).click();
                break;
        }*/
    }

    public void setTerm(String term) throws Exception {
        if (isTermDisplayed()) {
            Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#term " + Molecules.IBK_INPUTGROUP_TEXT)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                    .findElement(By.cssSelector("input")).sendKeys(term);
        }
    }

    private boolean isTermDisplayed() {
        try {
            WebElement term = getDriver().findElement(By.cssSelector("#term " + Molecules.IBK_INPUTGROUP_TEXT));
            return term.isDisplayed();
        } catch(Exception e) {
            return false;
        }
    }

    public void setOperation(String operation) {
        if (isOperationDisplayed()) {
            ((JavascriptExecutor) getDriver()).executeScript("window.scrollBy(0,95)");
            if (operation.equalsIgnoreCase("INDIVIDUALES")) {
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector(\"#accountType ibk-inputgroup-radio\")" +
                        ".shadowRoot.querySelector(\"label[for='styled-radio-typeaccount-2']\").click()");
            }
            if (operation.equalsIgnoreCase("CONJUNTAS")) {
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector(\"#accountType ibk-inputgroup-radio\")" +
                        ".shadowRoot.querySelector(\"label[for='styled-radio-typeaccount-3']\").click()");
            }
        }
    }

    private boolean isOperationDisplayed() {
        try {
            WebElement operation = getDriver().findElement(By.cssSelector("#accountType ibk-inputgroup-radio"));
            return operation.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setDebitCard(String debidCard) {
        if (isDebitCardDisplayed() && !debidCard.isEmpty()) {
            List<WebElement> cardViewElements = getDriver().findElements(By.cssSelector("app-debit-card-view " + Molecules.IBK_BUTTON_CREDIT_CARD));
            for (WebElement cardViewElement : cardViewElements) {
                WebElement debidCardElement = Util.expandRootElement(cardViewElement).findElement(By.cssSelector("div.ibkButtonCreditCard__name"));
                String debitCardName = debidCardElement.getText();
                System.out.println("Debit Card: " + debitCardName);
                if (debitCardName.equalsIgnoreCase(debidCard)) {
                    debidCardElement.click();
                    break;
                }
            }
        }
    }

    private boolean isDebitCardDisplayed() {
        try {
            WebElement debitCard =  getDriver().findElement(By.cssSelector("app-debit-card-view " + Molecules.IBK_BUTTON_CREDIT_CARD));
            return debitCard.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void validateDebitCardPrefix(String typeDebitCard) {
        String actualResult = Util.expandRootElement(Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("#debitCardNumber " + Molecules.IBK_INPUTGROUP_MASKED)))
                .findElement(By.cssSelector(Atoms.IBK_INPUT_MASKED)))
                .findElement(By.cssSelector("input")).getAttribute("value");
        switch (typeDebitCard) {
            case "Visa Débito\nClásica":
                Assert.assertEquals("El valor actual para la visa debito clasico " + actualResult + " no es igual al esperado 4213 55", "4213 55", actualResult);
                break;
            case "Visa Débito\nOro Chip":
                Assert.assertEquals("El valor actual para la visa debito otro chip " + actualResult + " no es igual al esperado 4547 77", "4547 77", actualResult);
                break;
            case "Visa Débito\nBenefit":
                Assert.assertEquals("El valor actual para la visa debito benefit " + actualResult + " no es igual al esperado 4110 74", "4110 74", actualResult);
                break;
        }
    }

    public void setDebitCardNumber(String debitCardNumber) {
        if (isDebitCardNumberDisplayed()) {
            Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#debitCardNumber " + Molecules.IBK_INPUTGROUP_MASKED)))
                    .findElement(By.cssSelector("div.IbkInputgroupCreditCard__Input " + Atoms.IBK_INPUT_MASKED)))
                    .findElement(By.cssSelector("input")).sendKeys(debitCardNumber);
        }
    }

    private String getErrorDebitCardNumber() {
        return Util.expandRootElement(Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("#debitCardNumber " + Molecules.IBK_INPUTGROUP_MASKED)))
                .findElement(By.cssSelector(Atoms.IBK_BREADCRUM)))
                .findElement(By.cssSelector("p")).getText();
    }

    public void validateDebitCardNumber() {
        Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("#create-account " + Atoms.IBK_BUTTON)))
                .findElement(By.cssSelector("button div div.text-container " + Particles.IBK_FONT)).click();
       // ApiCommons.waitFor(6);
        Assert.assertEquals("", "INGRESA UN NÚMERO DE TARJETA QUE EXISTA", getErrorDebitCardNumber());
    }

    private boolean isDebitCardNumberDisplayed() {
        try {
            WebElement debitCardNumber = getDriver()
                    .findElement(By.cssSelector("#debitCardNumber " + Molecules.IBK_INPUTGROUP_MASKED));
            return debitCardNumber.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setFirstSubProduct() {
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('ibk-child-product-card:nth-child(3)')" +
                ".shadowRoot.querySelectorAll('div ibk-font')[1].click()");
    }

    public void setSecondSubProduct() {
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('ibk-child-product-card:nth-child(4)')" +
                ".shadowRoot.querySelectorAll('div ibk-font')[1].click()");
    }

    public void setThirdSubProduct() {
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('ibk-child-product-card:nth-child(5)')" +
                ".shadowRoot.querySelectorAll('div ibk-font')[1].click()");
    }

    public void clickAffidavit() {
        ((JavascriptExecutor) getDriver()).executeScript("window.scrollBy(0,1200)");
        ((JavascriptExecutor)getDriver()).executeScript("document.querySelector('#accept ibk-inputgroup-radio')" +
                ".shadowRoot.querySelector(\"label[for='styled-radio-declaration0-1']\").click()");
    }
}
