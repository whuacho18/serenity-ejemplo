package pe.interbank.assi.front.ui;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Steps;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import pe.interbank.assi.common.utils.Util;
import pe.interbank.assi.front.ui.components.Atoms;
import pe.interbank.assi.front.ui.components.Molecules;
import pe.interbank.assi.front.ui.modals.ErrorServiceModalPage;

public class DashboardPage extends PageObject {

	@Steps
	ErrorServiceModalPage errorServiceModalPage;
	
	public void setSearch(String search) {
		if (isSearchDisplayed()) {
			WebElement txtSearch = Util.expandRootElement(Util.expandRootElement(getDriver()
					.findElement(By.cssSelector("#search " + Molecules.IBK_INPUTGROUP_TEXT)))
					.findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
					.findElement(By.cssSelector("div input"));
			Util.writeEachCharacter(txtSearch, search.split(""));
		}
	}

	private boolean isSearchDisplayed() {
		try {
			WebElement search = getDriver()
					.findElement(By.cssSelector("#search " + Molecules.IBK_INPUTGROUP_TEXT));
			return search.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public void clickOpenProductAvailable() {
		Util.expandRootElement(Util.expandRootElement(getDriver()
				.findElement(By.cssSelector("assi-products-available " + Molecules.IBK_PRODUCT_CARD)))
				.findElement(By.cssSelector("div " + Atoms.IBK_ICON_SVG)))
				.findElement(By.cssSelector("i svg")).click();
	}

	public void clickAccountType(String type) {
		if (isAccountTypeDisplayed()) {
			if (type.equals("INDIVIDUAL") || type.equals("MANCOMUNADO")) {
				if ("INDIVIDUAL".equals(type)) {
					((JavascriptExecutor) getDriver()).executeScript("return document.querySelector(\"assi-products-available ibk-product-card\")" +
							".shadowRoot.querySelector(\"div.type-account ibk-select\").shadowRoot.querySelector(\"#INDIVIDUAL\").click()");
				} else if ("MANCOMUNADO".equals(type)) {
					((JavascriptExecutor) getDriver()).executeScript("return document.querySelector(\"assi-products-available ibk-product-card\")" +
							".shadowRoot.querySelector(\"div.type-account ibk-select\").shadowRoot.querySelector(\"#MANCOMUNADO\").click()");
				}
			}
		}
	}

	private boolean isAccountTypeDisplayed() {
		try {
			WebElement accountType = getDriver().findElement(By.cssSelector("assi-products-available ibk-product-card"));
			return accountType.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public void clickButtonOpen() {
		int count = 1;
		Util.scrollTopPage();
		((JavascriptExecutor) getDriver()).executeScript("return document.querySelector('assi-products-available ibk-product-card')" +
				".shadowRoot.querySelector('div ibk-button').shadowRoot.querySelector('button ibk-font').shadowRoot.querySelector('p').click()");
		do {
			Util.waitFor(count);
			if (errorServiceModalPage.isUndestandButtonDisplayed()) {
				errorServiceModalPage.clickUnderstandButton();
				break;
			}
			count++;
		} while (!isExpedientDisplayed());
	}

	private boolean isExpedientDisplayed() {
		try {
			WebElement expedient = getDriver()
					.findElement(By.cssSelector(Atoms.IBK_H + "#numero"));
			return expedient.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

}
