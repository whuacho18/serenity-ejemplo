package pe.interbank.assi.front.ui;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Steps;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pe.interbank.assi.common.utils.Util;
import pe.interbank.assi.front.ui.components.Atoms;
import pe.interbank.assi.front.ui.components.Molecules;
import pe.interbank.assi.front.ui.components.Particles;
import pe.interbank.assi.front.ui.modals.ErrorServiceModalPage;

/**
 * @author Walter Huacho
 */
public class LoginPage extends PageObject {

	@Steps
	ErrorServiceModalPage errorServiceModalPage;
	
	public void setUser(String user) {
		Util.expandRootElement(Util.expandRootElement(getDriver()
				.findElement(By.cssSelector("app-custom-input[id='username'] " + Molecules.IBK_INPUTGROUP_TEXT)))
				.findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
				.findElement(By.cssSelector("div input")).sendKeys(user);
	}
	
	public void setPassword(String password) {
		Util.expandRootElement(Util.expandRootElement(getDriver()
				.findElement(By.cssSelector("app-custom-input[id='password'] " + Molecules.IBK_INPUTGROUP_TEXT)))
				.findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
				.findElement(By.cssSelector("div input")).sendKeys(password);
	}
	
	public void clickButtonLogin() {
		int count = 1;
		Util.expandRootElement(Util.expandRootElement(getDriver()
				.findElement(By.cssSelector("app-login " + Atoms.IBK_BUTTON)))
				.findElement(By.cssSelector("button " + Particles.IBK_FONT)))
				.findElement(By.cssSelector("p")).click();
		do {
			Util.waitFor(count);
			count++;
			if (errorServiceModalPage.isUndestandButtonDisplayed()) {
				errorServiceModalPage.clickUnderstandButton();
				break;
			}
		} while (!isSearchClientDisplayed());
	}

	private boolean isSearchClientDisplayed() {
		try {
			WebElement searchClient = getDriver().findElement(By.cssSelector("app-custom-input#docType " + Molecules.IBK_INPUTGROUP_SELECT));
			return searchClient.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public String getTitlePage() {
		return Util.expandRootElement(getDriver().findElement(By.cssSelector("app-login " + Atoms.IBK_H)))
				.findElement(By.cssSelector("p")).getText();
	}

	public Boolean isDisplayedInvalidUserMessage() {
		try {
			WebElement invalidUserMessage = Util.expandRootElement(getDriver()
					.findElement(By.cssSelector("#username " + Molecules.IBK_INPUTGROUP_TEXT)))
					.findElement(By.cssSelector(Atoms.IBK_BREADCRUM));
			return invalidUserMessage.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public Boolean isDisplayedInvalidPasswordMessage() {
		try {
			WebElement invalidPasswordMessage = Util.expandRootElement(getDriver()
					.findElement(By.cssSelector("#password " + Molecules.IBK_INPUTGROUP_TEXT)))
					.findElement(By.cssSelector(Atoms.IBK_BREADCRUM));
			return invalidPasswordMessage.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
}
