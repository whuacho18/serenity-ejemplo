package pe.interbank.assi.front.ui;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import pe.interbank.assi.common.utils.Util;
import pe.interbank.assi.front.ui.components.Atoms;
import pe.interbank.assi.front.ui.components.Molecules;
import pe.interbank.assi.front.ui.components.Particles;

public class PersonalInfoSecondAssociatePage extends PageObject {

    public void setDocType(String docType) {
        ((JavascriptExecutor)getDriver()).executeScript("window.scrollBy(0,500)");
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector(\"#views-container " +
                "div:nth-child(4) app-search-client-form app-custom-input#docType ibk-inputgroup-select\")" +
                ".shadowRoot.querySelector(\""+ Atoms.IBK_SELECT+"\").shadowRoot.querySelector(\"#"+docType+"\").click()");
    }


    public void setDoc(String doc) {
        Util.expandRootElement(Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("#views-container div:nth-child(4) app-search-client-form app-custom-input#doc " + Molecules.IBK_INPUTGROUP_TEXT)))
                .findElement(By.cssSelector(Atoms.IBK_INPUT)))
                .findElement(By.cssSelector("input")).sendKeys(doc);
    }

    public void clickButtonSearchCustomer() {
        int count = 1;
        Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("#views-container div:nth-child(4) app-search-client-form " + Atoms.IBK_BUTTON)))
                .findElement(By.cssSelector(Particles.IBK_FONT)).click();
/*        do {
            ApiCommons.waitFor(count);
            count++;
        } while(!isEmailDisplayed());
        ((JavascriptExecutor)getDriver()).executeScript("window.scrollBy(0,50)");*/
    }

    public void setFirstName(String firstName) {
       // ApiCommons.waitFor(3);
        if (isFirstNameDisplayed()) {
            WebElement firstNameElement = Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#views-container div:nth-child(4) app-associate-view #firstName " + Molecules.IBK_INPUTGROUP_TEXT)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                    .findElement(By.cssSelector("input"));
            if (firstNameElement.getAttribute("value").isEmpty()) {
                firstNameElement.sendKeys(firstName);
            }
       }
    }

    private boolean isFirstNameDisplayed() {
        try {
            WebElement firstName = getDriver().findElement(By.cssSelector("#views-container div:nth-child(4) app-associate-view #firstName " + Molecules.IBK_INPUTGROUP_TEXT));
            return firstName.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setSecondName(String secondName) {
        if (isSecondNameDisplayed()) {
            WebElement secondNameElement = Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#views-container div:nth-child(4) app-associate-view #secondName " + Molecules.IBK_INPUTGROUP_TEXT)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                    .findElement(By.cssSelector("input"));
            if (secondNameElement.getAttribute("value").isEmpty()) {
                secondNameElement.sendKeys(secondName);
            }
        }
    }

    private boolean isSecondNameDisplayed() {
        try {
            WebElement secondName = getDriver().findElement(By.cssSelector("#views-container div:nth-child(4) app-associate-view #secondName " + Molecules.IBK_INPUTGROUP_TEXT));
            return secondName.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setLastName(String lastName) {
        if (isLastNameDisplayed()) {
            WebElement lastNameElement = Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#views-container div:nth-child(4) app-associate-view #lastName " + Molecules.IBK_INPUTGROUP_TEXT)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                    .findElement(By.cssSelector("input"));
            if (lastNameElement.getAttribute("value").isEmpty()) {
                lastNameElement.sendKeys(lastName);
            }
        }
    }

    private boolean isLastNameDisplayed() {
        try {
            WebElement lastName = getDriver().findElement(By.cssSelector("#views-container div:nth-child(4) app-associate-view #lastName " + Molecules.IBK_INPUTGROUP_TEXT));
            return lastName.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setMotherLastName(String motherLastName) {
        if (isMotherLastNameDisplayed()) {
            WebElement motherLastNameElement = Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#views-container div:nth-child(4) app-associate-view #motherLastName " + Molecules.IBK_INPUTGROUP_TEXT)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                    .findElement(By.cssSelector("input"));
            if (motherLastNameElement.getAttribute("value").isEmpty()) {
                motherLastNameElement.sendKeys(motherLastName);
            }
        }
    }

    private boolean isMotherLastNameDisplayed() {
        try {
            WebElement motherLastName = getDriver().findElement(By.cssSelector("#views-container div:nth-child(4) app-associate-view #motherLastName " + Molecules.IBK_INPUTGROUP_TEXT));
            return motherLastName.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setEmail(String email) {
        if (isEmailDisplayed()) {
            WebElement emailElement = Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#views-container div:nth-child(4) app-associate-view #email " + Molecules.IBK_INPUTGROUP_TEXT)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                    .findElement(By.cssSelector("input"));
            //if (emailElement.getAttribute("value").isEmpty()) {
                Util.writeEachCharacter(emailElement, email.split(""));
           //}
           // this.globalData.setData("vOutEmail", email);
        }
    }

    private boolean isEmailDisplayed() {
        try {
            WebElement email = getDriver().findElement(By.cssSelector("#views-container div:nth-child(4) app-associate-view #email " + Molecules.IBK_INPUTGROUP_TEXT));
            return email.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setCarrier(String carrier) {
        if (isCarrierDisplayed()) {
            ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector(\"#views-container div:nth-child(4) app-associate-view #oper ibk-inputgroup-select\")" +
                    ".shadowRoot.querySelector(\"div ibk-select\").shadowRoot.querySelector(\"#" + carrier + "\").click()");
          //  this.globalData.setData("vOutCarrier", carrier);
        }
    }

    private boolean isCarrierDisplayed() {
        try {
            WebElement carrier = getDriver().findElement(By.cssSelector("#views-container div:nth-child(4) app-associate-view #oper " + Molecules.IBK_INPUTGROUP_SELECT));
            return carrier.isDisplayed();
        } catch(Exception e) {
            return false;
        }
    }

    public void setPhoneNumber(String phoneNumber) {
        if (isPhoneNumberDisplayed()) {
            ((JavascriptExecutor)getDriver()).executeScript("window.scrollBy(0,100)");
            WebElement phoneElement = Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#views-container div:nth-child(4) app-associate-view #movil " + Molecules.IBK_INPUTGROUP_TEXT)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                    .findElement(By.cssSelector("input"));
            if (!phoneElement.getText().isEmpty()) {
                //this.globalData.setData("vOutPhoneNumber", phoneNumber);
            }

            if (phoneElement.getAttribute("value").isEmpty()) {
                phoneElement.sendKeys(phoneNumber);
           //     this.globalData.setData("vOutPhoneNumber", phoneNumber);
            }
        }
    }

    private boolean isPhoneNumberDisplayed() {
        try {
            WebElement phone = getDriver().findElement(By.cssSelector("#views-container div:nth-child(4) app-associate-view #movil " + Molecules.IBK_INPUTGROUP_TEXT));
            return phone.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setBirthDate(String birthDate) {
        if (isBirthDateDisplayed()) {
            WebElement birthDateElement = Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#views-container div:nth-child(4) app-associate-view #birthday " + Molecules.IBK_INPUTGROUP_TEXT)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_INPUT_DATE)))
                    .findElement(By.cssSelector("input"));
            if (birthDateElement.getAttribute("value").isEmpty()) {
                Util.writeEachCharacter(birthDateElement, birthDate.split(""));
            }
        }
    }

    private boolean isBirthDateDisplayed() {
        try {
            WebElement birthDate = getDriver().findElement(By.cssSelector("#views-container div:nth-child(4) app-associate-view #birthday " + Molecules.IBK_INPUTGROUP_TEXT));
            return birthDate.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setMaritalStatus(String maritalStatus) {
        if (isMaritalStatusDisplayed()) {
            ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector(\"#views-container div:nth-child(4) app-associate-view #marital ibk-inputgroup-select\")" +
                    ".shadowRoot.querySelector(\"div ibk-select\").shadowRoot.querySelector(\"#" + maritalStatus + "\").click()");
        }
    }

    private boolean isMaritalStatusDisplayed() {
        try {
            WebElement marital = getDriver().findElement(By.cssSelector("#views-container div:nth-child(4) app-associate-view #marital " + Molecules.IBK_INPUTGROUP_SELECT));
            return marital.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setGender(String gender) {
        if (isGenderDisplayed()) {
            if (gender.equals("MASCULINO")) {
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector(\"#views-container div:nth-child(4) app-associate-view #gender ibk-inputgroup-tabs\")" +
                        ".shadowRoot.querySelector(\"div div div ibk-tab\").shadowRoot.querySelector(\"div div:nth-child(1)" +
                        " button div.font-container p\").click()");
            } else if (gender.equals("FEMENINO")) {
                ((JavascriptExecutor) getDriver()).executeScript("return document.querySelector(\"#views-container div:nth-child(4) app-associate-view #gender  ibk-inputgroup-tabs\")" +
                        ".shadowRoot.querySelector(\"div div div ibk-tab\").shadowRoot.querySelector(\"div div:nth-child(2)" +
                        " button div.font-container p\").click()");
            }
        }
    }

    private boolean isGenderDisplayed() {
        try {
            WebElement gender = getDriver().findElement(By.cssSelector("#views-container div:nth-child(4) app-associate-view #gender " + Molecules.IBK_INPUTGROUP_TABS));
            return gender.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setOccupation(String occupation) {
        if (isOccupationDisplayed()) {
            WebElement occupationElement = Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#views-container div:nth-child(4) app-associate-view #occup " + Molecules.IBK_AUTOCOMPLETEGROUP_TEXT_CPV)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_TEXTFIELD_AUTOCOMPLETE_CPV)))
                    .findElement(By.cssSelector("input"));
            if (occupationElement.getAttribute("value").isEmpty()) {
                occupationElement.sendKeys(occupation);
            }
        }
    }

    private boolean isOccupationDisplayed() {
        try {
            WebElement occupation = getDriver().findElement(By.cssSelector("#views-container div:nth-child(4) app-associate-view #occup " + Molecules.IBK_AUTOCOMPLETEGROUP_TEXT_CPV));
            return occupation.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setBusinessRuc(String ruc) {
        if (isBusinessRucDisplayed()) {
            WebElement businessRucElement = Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#views-container div:nth-child(4) app-associate-view #businessRuc " + Molecules.IBK_INPUTGROUP_TEXT)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                    .findElement(By.cssSelector("input"));
            if (businessRucElement.getAttribute("value").isEmpty()) {
                businessRucElement.sendKeys(ruc);
            }

            Util.expandRootElement(Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#views-container div:nth-child(4) app-associate-view #businessRuc " + Molecules.IBK_INPUTGROUP_TEXT)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_ICON_SVG)))
                    .findElement(By.cssSelector("i svg")).click();
            //ApiCommons.waitFor(3);
        }
    }

    private boolean isBusinessRucDisplayed() {
        try {
            WebElement businessRuc = getDriver().findElement(By.cssSelector("#views-container div:nth-child(3) app-associate-view #businessRuc " + Molecules.IBK_INPUTGROUP_TEXT));
            return businessRuc.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setBusinessName(String businessName) {
      //  ApiCommons.waitFor(2);
        if (isDisplayedBusinessRucBreadCrum()) {
            Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#views-container div:nth-child(4) app-associate-view #businessName " + Molecules.IBK_INPUTGROUP_TEXT)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                    .findElement(By.cssSelector("input")).sendKeys(businessName);
        }
    }

    private boolean isDisplayedBusinessRucBreadCrum() {
        try {
            WebElement businessRucBreadCrum = getDriver()
                    .findElement(By.cssSelector("#views-container div:nth-child(4) app-associate-view #no-found-ruc " + Atoms.IBK_BREADCRUM));
            return businessRucBreadCrum.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setIndependent() {
        if (isIndependentDisplayed()) {
            Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#views-container div:nth-child(4) " + Atoms.IBK_CHECKBOX)))
                    .findElement(By.cssSelector("div label")).click();
        }
    }

    private boolean isIndependentDisplayed() {
        try {
            WebElement independentCheckbox = getDriver().findElement(By.cssSelector("#views-container div:nth-child(4) " + Atoms.IBK_CHECKBOX));
            return independentCheckbox.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setSpouseDocumentType(String documentType) {
        if (isSpouseFormDisplayed()) {
            ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector(\"app-spouse-form #docType " +
                    "ibk-inputgroup-select\").shadowRoot.querySelector(\"div ibk-select\")" +
                    ".shadowRoot.querySelector(\"#"+documentType+"\").click()");
        }
    }

    public void setSpouseDocumentNumber(String doc) {
        if (isSpouseFormDisplayed()) {
            Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("app-spouse-form #doc " + Molecules.IBK_INPUTGROUP_TEXT)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                    .findElement(By.cssSelector("input")).sendKeys(doc);
        }
    }

    public void setSpouseNames(String spouseNames) {
        if (isSpouseFormDisplayed()) {
            Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("app-spouse-form #spouseNames " + Molecules.IBK_INPUTGROUP_TEXT)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                    .findElement(By.cssSelector("input")).sendKeys(spouseNames);
        }
    }

    public void setSpouseSurNames(String surNames) {
        if (isSpouseFormDisplayed()) {
            Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("app-spouse-form #surNames " + Molecules.IBK_INPUTGROUP_TEXT)))
                    .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                    .findElement(By.cssSelector("input")).sendKeys(surNames);
        }
    }

    private boolean isSpouseFormDisplayed() {
        try {
            WebElement spouseForm = getDriver().findElement(By.cssSelector("app-spouse-form"));
            return spouseForm.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void clickAddPersonLink() {
        Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("#add-person div.link-bottom.ng-star-inserted " + Atoms.IBK_LINK)))
                .findElement(By.cssSelector("p")).click();
    }

    public void clickButtonNext() {
        if (isButtonNextDisplayed()) {
            Util.expandRootElement(Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector("#row-buttons " + Atoms.IBK_BUTTON)))
                    .findElement(By.cssSelector("button " + Particles.IBK_FONT)))
                    .findElement(By.cssSelector("p")).click();
        }
    }

    private boolean isButtonNextDisplayed() {
        try {
            WebElement buttonNext = getDriver()
                    .findElement(By.cssSelector("#row-buttons " + Atoms.IBK_BUTTON));
            return buttonNext.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }
}
