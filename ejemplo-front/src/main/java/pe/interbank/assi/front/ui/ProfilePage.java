package pe.interbank.assi.front.ui;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import pe.interbank.assi.front.ui.components.Atoms;

public class ProfilePage extends PageObject {
    
    public void clickProfileIcon() {
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#right ibk-menu')" +
                ".shadowRoot.querySelector('div div.icon span ibk-h').shadowRoot.querySelector('p').click()");
    }

    public void clickReportsOption() {
    //    int count = 1;
        ((JavascriptExecutor)getDriver()).executeScript("return document" +
                ".querySelector(\"#right div.ibk-flex.text-container.vertical-center ibk-menu\")" +
                ".shadowRoot.querySelector(\"#inbox div.menu-elements div.notification div:nth-child(2) ibk-menu-item\")" +
                ".shadowRoot.querySelector(\"ibk-h\").shadowRoot.querySelector(\"p\").click()");
/*        do {
            ApiCommons.waitFor(count);
            count++;
        } while (!isReportTitleDisplayed());*/
    }

    private boolean isReportTitleDisplayed() {
        try {
            WebElement reportTitle = getDriver().findElement(By.cssSelector("assi-reports " + Atoms.IBK_H));
            return reportTitle.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }
}
