package pe.interbank.assi.front.ui;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import pe.interbank.assi.common.utils.Util;

public class ReviewPage extends PageObject {
    
    private static final String TABLE_REVIEW_ACCOUNT_NUMBER = "#table-container table tbody tr:nth-child(2) td:nth-child(2)";

    public void getAccountNumber() {
        String accountNumber;
    //    if (Arrays.asList("PLAZO", "CASA", "TECHO", "CERTIFICADO").contains(this.globalData.getData("viSubProduct"))) {
            accountNumber = getDriver().findElement(By.cssSelector(TABLE_REVIEW_ACCOUNT_NUMBER + " span")).getText();
            //this.globalData.setData("vOutAccountNumber", accountNumber);
       // } else {
            accountNumber = Util.expandRootElement(getDriver()
                    .findElement(By.cssSelector(TABLE_REVIEW_ACCOUNT_NUMBER + " div ibk-h"))).findElement(By.cssSelector("p")).getText();
            int size = accountNumber.length();
            //this.globalData.setData("vOutAccountNumber", accountNumber.substring(size-13, size));
     //   }
    }

}
