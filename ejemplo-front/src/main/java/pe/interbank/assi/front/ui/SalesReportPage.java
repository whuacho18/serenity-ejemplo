package pe.interbank.assi.front.ui;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import pe.interbank.assi.common.utils.PropertyManager;
import pe.interbank.assi.common.utils.Util;
import pe.interbank.assi.front.ui.components.Atoms;
import pe.interbank.assi.front.ui.components.Molecules;
import pe.interbank.assi.front.ui.components.Particles;

public class SalesReportPage extends PageObject {

    public void clickPromoter() {
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector(\"#promoterDropdown " +
                "ibk-inputgroup-select\").shadowRoot.querySelector(\"div ibk-select\")" +
                ".shadowRoot.querySelector(\"#"+ PropertyManager.getInstance().getTokenUser() +"\").click()");
    }

    public void clickSearch() {
      //  int count = 1;
        Util.expandRootElement(getDriver().findElement(By.cssSelector(Atoms.IBK_BUTTON)))
               .findElement(By.cssSelector("button " + Particles.IBK_FONT)).click();
/*        do {
            ApiCommons.waitFor(count);
            count++;
        } while (!isSearchResultsSubtitleDisplayed());
        ((JavascriptExecutor)getDriver()).executeScript("window.scrollBy(0,400)");*/
    }

    private boolean isSearchResultsSubtitleDisplayed() {
        try {
            WebElement searchResultsSubtitle = getDriver()
                    .findElement(By.cssSelector("assi-reports div:nth-child(2) " + Atoms.IBK_H));
            return searchResultsSubtitle.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void clickRightTable() {
        Util.expandRootElement(Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("assi-reports " + Molecules.IBK_GRID_GROUP)))
                .findElement(By.cssSelector("div.vector-aside.right " + Atoms.IBK_ICON_SVG)))
                .findElement(By.cssSelector("i")).click();
    }

    public void clickLeftTable() {
        Util.expandRootElement(Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("assi-reports " + Molecules.IBK_GRID_GROUP)))
                .findElement(By.cssSelector("div.vector-aside.left " + Atoms.IBK_ICON_SVG)))
                .findElement(By.cssSelector("i")).click();
    }

    public void clickAnullmentAccount() {
        int count = 1;
        Util.expandRootElement(Util.expandRootElement(Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("assi-reports " + Molecules.IBK_GRID_GROUP)))
                .findElement(By.cssSelector("table tbody tr:nth-child(1) td.td-icons div div:nth-child(2) " + Atoms.IBK_SVG_BACKGROUND)))
                .findElement(By.cssSelector("div " + Atoms.IBK_SVG)))
                .findElement(By.cssSelector("i svg")).click();
/*        do {
            ApiCommons.waitFor(count);
            count++;
        } while (!isAnnulmentModalDisplayed());*/
    }

    public boolean isAnnulmentModalDisplayed() {
        try {
            WebElement annulmentModal = getDriver().findElement(By.cssSelector("app-annulation-modal " + Atoms.IBK_H));
            return annulmentModal.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }
}
