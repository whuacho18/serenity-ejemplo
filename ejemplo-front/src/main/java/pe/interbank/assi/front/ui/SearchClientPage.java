package pe.interbank.assi.front.ui;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Steps;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pe.interbank.assi.common.utils.Util;
import pe.interbank.assi.front.ui.components.Atoms;
import pe.interbank.assi.front.ui.components.Molecules;
import pe.interbank.assi.front.ui.modals.ErrorServiceModalPage;

/**
 * @author Walter Huacho
 */
public class SearchClientPage extends PageObject {
	@Steps
	ErrorServiceModalPage errorServiceModalPage;

	public void setDocumentType(String type) {
		WebElement abrirComboBox = Util.expandRootElement(Util.expandRootElement(getDriver()
				.findElement(By.cssSelector("app-custom-input#docType "+ Molecules.IBK_INPUTGROUP_SELECT)))
				.findElement(By.cssSelector("div " + Atoms.IBK_SELECT)))
				.findElement(By.cssSelector("div div"));
		abrirComboBox.click();
		WebElement cboTipoDocumento = Util.expandRootElement(Util.expandRootElement(getDriver()
				.findElement(By.cssSelector("app-custom-input#docType "+ Molecules.IBK_INPUTGROUP_SELECT)))
				.findElement(By.cssSelector("div " + Atoms.IBK_SELECT)))
				.findElement(By.cssSelector("div ul li#" + type + ""));
		cboTipoDocumento.click();
	}

	public void setDocumentNumber(String number) {
		WebElement lblNroDocumento = Util.expandRootElement(Util.expandRootElement(getDriver()
				.findElement(By.cssSelector("app-custom-input#doc " + Molecules.IBK_INPUTGROUP_TEXT)))
				.findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
				.findElement(By.cssSelector("div input"));
		lblNroDocumento.sendKeys(number);
	}

	public void clickButtonValidate() {
		int count = 1;
		Util.expandRootElement(getDriver()
				.findElement(By.cssSelector(Atoms.IBK_BUTTON)))
				.findElement(By.cssSelector("button")).click();
		do {
			Util.waitFor(count);
			if (errorServiceModalPage.isUndestandButtonDisplayed()) {
				errorServiceModalPage.clickUnderstandButton();
				break;
			}
			count++;
		} while (!isSearchProductDisplayed());
	}

	private boolean isSearchProductDisplayed() {
		try {
			WebElement search = getDriver()
					.findElement(By.cssSelector("#channell"));
			return search.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public String getTitlePage() {
		return Util.expandRootElement(getDriver()
				.findElement(By.cssSelector("app-search-client " + Atoms.IBK_H)))
				.findElement(By.cssSelector("p")).getText();
	}
}
