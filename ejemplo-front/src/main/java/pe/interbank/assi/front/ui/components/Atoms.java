package pe.interbank.assi.front.ui.components;

/**
 * @author Walter Huacho
 */
public class Atoms {
    public static final String IBK_AVATAR = "ibk-avatar";
    public static final String IBK_BADGE = "ibk-badge";
    public static final String IBK_BREADCRUM = "ibk-breadcrum";
    public static final String IBK_BUTTON_TOGGLE = "ibk-button-toggle";
    public static final String IBK_BUTTON = "ibk-button";
    public static final String IBK_CHECKBOX = "ibk-checkbox";
    public static final String IBK_CONTENT_CARD = "ibk-content-card";
    public static final String IBK_H = "ibk-h";
    public static final String IBK_SVG = "ibk-svg";
    public static final String IBK_ICON_SVG = "ibk-icon-svg";
    public static final String IBK_SVG_BACKGROUND = "ibk-svg-background";
    public static final String IBK_ICON_TOOLTIP = "ibk-icon-tooltip";
    public static final String IBK_ICON = "ibk-icon";
    public static final String IBK_IMAGE_SVG = "ibk-image-svg";
    public static final String IBK_INPUT_AMOUNT = "ibk-input-amount";
    public static final String IBK_INPUT_DATE = "ibk-input-date";
    public static final String IBK_INPUT_MASK = "ibk-input-mask";
    public static final String IBK_INPUT_MASKED = "ibk-input-masked";
    public static final String IBK_INPUT = "ibk-input";
    public static final String IBK_LABEL_BADGE = "ibk-label-badge";
    public static final String IBK_LABEL = "ibk-label";
    public static final String IBK_LI = "ibk-li";
    public static final String IBK_LINK = "ibk-link";
    public static final String IBK_LOADER = "ibk-loader";
    public static final String IBK_P = "ibk-p";
    public static final String IBK_PAGINATION = "ibk-pagination";
    public static final String IBK_PAGINATION2 = "ibk-pagination2";
    public static final String IBK_SELECT = "ibk-select";
    public static final String IBK_TAB = "ibk-tab";
    public static final String IBK_TABS = "ibk-tabs";
    public static final String IBK_TEXTAREA = "ibk-textarea";
    public static final String IBK_TEXTFIELD_AUTOCOMPLETE = "ibk-textfield-autocomplete";
    public static final String IBK_TEXTFIELD_AUTOCOMPLETE_CPV = "ibk-textfield-autocomplete-cpv";
    public static final String IBK_INPUT_AUTOCOMPLETE = "ibk-input-autocomplete";
    public static final String IBK_MENU_ITEM = "ibk-menu-item";
    public static final String IBK_TOAST = "ibk-toast";
}
