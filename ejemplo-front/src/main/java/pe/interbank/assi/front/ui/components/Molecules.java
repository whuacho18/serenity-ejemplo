package pe.interbank.assi.front.ui.components;

/**
 * @author Walter Huacho
 */
public class Molecules {
    public static final String IBK_AUTOCOMPLETEGROUP_TEXT = "ibk-autocompletegroup-text";
    public static final String IBK_AUTOCOMPLETEGROUP_TEXT_CPV = "ibk-autocompletegroup-text-cpv";
    public static final String IBK_INPUTGROUP_AUTOCOMPLETE = "ibk-inputgroup-autocomplete";
    public static final String IBK_CREDIT_CARD = "ibk-credit-card";
    public static final String IBK_LOAN_CALCULATION_CARD = "ibk-loan-calculation-card";
    public static final String IBK_BUTTON_CREDIT_CARD = "ibk-button-credit-card";
    public static final String IBK_BUTTON_ICON = "ibk-button-icon";
    public static final String IBK_CAMPAIGN_CARD = "ibk-campaign-card";
    public static final String IBK_CUSTOMER_PROFILE_UXRF = "ibk-customer-profile-uxrf";
    public static final String IBK_CUSTOMER_PROFILE = "ibk-customer-profile";
    public static final String IBK_GRID_GROUP = "ibk-grid-group";
    public static final String IBK_INPUTGROUP_AMOUNT = "ibk-inputgroup-amount";
    public static final String IBK_INPUTGROUP_MASK = "ibk-inputgroup-mask";
    public static final String IBK_INPUTGROUP_MASKED = "ibk-inputgroup-masked";
    public static final String IBK_INPUTGROUP_RADIO = "ibk-inputgroup-radio";
    public static final String IBK_INPUTGROUP_SELECT = "ibk-inputgroup-select";
    public static final String IBK_INPUTGROUP_TABS = "ibk-inputgroup-tabs";
    public static final String IBK_INPUTGROUP_TEXT = "ibk-inputgroup-text";
    public static final String IBK_INPUTGROUP_TEXTAREA = "ibk-inputgroup-textarea";
    public static final String IBK_MENU = "ibk-menu";
    public static final String IBK_INBOX_NOTIFICATIONS = "ibk-inbox-notifications";
    public static final String IBK_PAGINATION_GROUP = "ibk-pagination-group";
    public static final String IBK_PAGINATION_GROUP2 = "ibk-pagination-group2";
    public static final String IBK_PRODUCT_CARD = "ibk-product-card";
    public static final String IBK_RADIO_CARD = "ibk-radio-card";
    public static final String IBK_TABLE = "ibk-table";
    public static final String IBK_TOAST = "ibk-toast";
}
