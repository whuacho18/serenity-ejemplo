package pe.interbank.assi.front.ui.components;

/**
 * @author Walter Huacho
 */
public class Organisms {
    public static final String IBK_GRID_GROUP = "ibk-grid-group";
    public static final String IBK_INBOX_NOTIFICATIONS = "ibk-inbox-notifications";
    public static final String IBK_MENU = "ibk-menu";
    public static final String IBK_TABLE_GROUP = "ibk-table-group";
}
