package pe.interbank.assi.front.ui.modals;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import pe.interbank.assi.common.utils.Util;
import pe.interbank.assi.front.ui.components.Atoms;
import pe.interbank.assi.front.ui.components.Molecules;
import pe.interbank.assi.front.ui.components.Particles;

public class AnnulmentModalPage extends PageObject {

    public void setAnnulmentMotive() {
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelector('#channell')" +
                ".shadowRoot.querySelector('div ibk-select').shadowRoot.querySelector('li:nth-child(1) span').click()");
    }

    public void setDescription(String description) {
        Util.expandRootElement(Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("#description " + Molecules.IBK_INPUTGROUP_TEXTAREA)))
                .findElement(By.cssSelector("div " + Atoms.IBK_TEXTAREA)))
                .findElement(By.cssSelector("textarea")).sendKeys(description);
    }

    public void clickNextButton() {
/*        int count = 1;*/
        Util.expandRootElement(Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("app-annulation-modal " + Atoms.IBK_BUTTON)))
                .findElement(By.cssSelector("button " + Particles.IBK_FONT)))
                .findElement(By.cssSelector("p")).click();
/*        do {
            ApiCommons.waitFor(count);
            count++;
        } while (!isUserDisplayed());*/
    }

    private boolean isUserDisplayed() {
        try {
            WebElement user = getDriver()
                    .findElement(By.cssSelector("#username " + Molecules.IBK_INPUTGROUP_TEXT));
            return user.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setUser(String user) {
        Util.expandRootElement(Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("#username " + Molecules.IBK_INPUTGROUP_TEXT)))
                .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                .findElement(By.cssSelector("div input")).sendKeys(user);
    }

    public void setPassword(String password) {
        Util.expandRootElement(Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("#password " + Molecules.IBK_INPUTGROUP_TEXT)))
                .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                .findElement(By.cssSelector("div input")).sendKeys(password);
    }

    public void clickSendButton() throws Exception {
/*        int count = 1;*/
        Util.expandRootElement(Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("app-annulation-modal " + Atoms.IBK_BUTTON)))
                .findElement(By.cssSelector("button " + Particles.IBK_FONT)))
                .findElement(By.cssSelector("p")).click();
/*        do {
            ApiCommons.waitFor(count);
            count++;
            if (this.errorServiceModalPage.isUndestandButtonDisplayed()) {
                this.errorServiceModalPage.clickUnderstandButton();
                break;
            }
        } while (!isDisplayedToast());*/
    }

    private boolean isDisplayedToast() {
        try {
            WebElement toast = getDriver().findElement(By.cssSelector(Atoms.IBK_TOAST));
            return toast.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }
}
