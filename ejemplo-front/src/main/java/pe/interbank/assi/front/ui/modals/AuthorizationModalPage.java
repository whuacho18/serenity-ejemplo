package pe.interbank.assi.front.ui.modals;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pe.interbank.assi.common.utils.Util;
import pe.interbank.assi.front.ui.components.Atoms;
import pe.interbank.assi.front.ui.components.Molecules;
import pe.interbank.assi.front.ui.components.Particles;

/**
 * @author Walter Huacho
 */
public class AuthorizationModalPage extends PageObject {

    public void setUserCreditCard(String user) {
/*        int count = 1;*/
        Util.expandRootElement(Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("cc-authorization-modal div:nth-child(2) " + Molecules.IBK_INPUTGROUP_TEXT)))
                .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                .findElement(By.cssSelector("input")).sendKeys(user);
/*        do {
            ApiCommons.waitFor(count);
            count++;
        } while (!isUserCreditCardDisplayed());*/
    }

    private boolean isUserCreditCardDisplayed() {
        try {
            WebElement user = getDriver()
                    .findElement(By.cssSelector("cc-authorization-modal div:nth-child(2) " + Molecules.IBK_INPUTGROUP_TEXT));
            return user.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setPasswordCreditCard(String password) {
/*        int count = 1;*/
        Util.expandRootElement(Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("cc-authorization-modal div:nth-child(3) " + Molecules.IBK_INPUTGROUP_TEXT)))
                .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                .findElement(By.cssSelector("input")).sendKeys(password);
/*        do {
            ApiCommons.waitFor(count);
            count++;
        } while (!isPasswordCreditCardDisplayed());*/
    }

    private boolean isPasswordCreditCardDisplayed() {
        try {
            WebElement password = getDriver()
                    .findElement(By.cssSelector("cc-authorization-modal div:nth-child(3) " + Molecules.IBK_INPUTGROUP_TEXT));
            return password.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setUserLoan(String user) {
/*        int count = 1;*/
        Util.expandRootElement(Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("pl-disbursement-authorization-modal div:nth-child(2) " + Molecules.IBK_INPUTGROUP_TEXT)))
                .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                .findElement(By.cssSelector("input")).sendKeys(user);
/*        do {
            ApiCommons.waitFor(count);
            count++;
        } while (!isUserLoanDisplayed());*/
    }

    private boolean isUserLoanDisplayed() {
        try {
            WebElement user = getDriver()
                    .findElement(By.cssSelector("pl-disbursement-authorization-modal div:nth-child(2) " + Molecules.IBK_INPUTGROUP_TEXT));
            return user.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void setPasswordLoan(String password) {
/*        int count = 1;*/
        Util.expandRootElement(Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("pl-disbursement-authorization-modal div:nth-child(3) " + Molecules.IBK_INPUTGROUP_TEXT)))
                .findElement(By.cssSelector("div " + Atoms.IBK_INPUT)))
                .findElement(By.cssSelector("input")).sendKeys(password);
/*        do {
            ApiCommons.waitFor(count);
            count++;
        } while (!isPasswordLoanDisplayed());*/
    }

    private boolean isPasswordLoanDisplayed() {
        try {
            WebElement password = getDriver()
                    .findElement(By.cssSelector("pl-disbursement-authorization-modal div:nth-child(3) " + Molecules.IBK_INPUTGROUP_TEXT));
            return password.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void clickButtonAuthorization() {
        Util.expandRootElement(Util.expandRootElement(Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("div div.Modal-body.Modal--fields form div.grid.mt-40 div.col.col-end " + Atoms.IBK_BUTTON)))
                .findElement(By.cssSelector("button div div.text-container " + Particles.IBK_FONT))))
                .findElement(By.cssSelector("p"))
                .click();
    }
}
