package pe.interbank.assi.front.ui.modals;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import pe.interbank.assi.common.utils.Util;
import pe.interbank.assi.front.ui.components.Atoms;
import pe.interbank.assi.front.ui.components.Particles;

/**
 * @author Walter Huacho
 */
public class ChannelModalPage extends PageObject {

    public void setChannel(String channel) {
        String jsSelectorChannel = "return document.querySelector('#channell').shadowRoot.querySelector('ibk-select')";
        if (channel.equalsIgnoreCase("TIENDAS LIMA")) {
            ((JavascriptExecutor) getDriver()).executeScript(jsSelectorChannel + ".shadowRoot.querySelectorAll('li')['0'].click()");
        }
        if (channel.equalsIgnoreCase("TELEVENTAS")) {
            ((JavascriptExecutor) getDriver()).executeScript(jsSelectorChannel + ".shadowRoot.querySelectorAll('li')['1'].click()");
        }
    }

    public void clickButtonSave() {
        Util.expandRootElement(Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("app-channel-modal " + Atoms.IBK_BUTTON)))
                .findElement(By.cssSelector("button " + Particles.IBK_FONT)))
                .findElement(By.cssSelector("p")).click();
    }
}
