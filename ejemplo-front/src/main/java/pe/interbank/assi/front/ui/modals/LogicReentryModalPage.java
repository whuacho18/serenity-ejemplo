package pe.interbank.assi.front.ui.modals;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pe.interbank.assi.common.utils.Util;
import pe.interbank.assi.front.ui.components.Atoms;
import pe.interbank.assi.front.ui.components.Particles;

/**
 * @author Walter Huacho
 */
public class LogicReentryModalPage extends PageObject {
    
    public void clickUnderstandButtonCreditCard() {
        Util.expandRootElement(Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("cc-reentry-logic-modal " + Atoms.IBK_BUTTON)))
                .findElement(By.cssSelector("button " + Particles.IBK_FONT)))
                .findElement(By.cssSelector("p")).click();
    }

    public boolean isUnderstandButtonCreditCardDisplayed() {
        try {
            WebElement restrictionModalElement = getDriver().findElement(By.cssSelector("cc-reentry-logic-modal " + Atoms.IBK_BUTTON));
            return restrictionModalElement.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void clickUnderstandButtonLoan() {
        Util.expandRootElement(Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("pl-reentry-logic-modal " + Atoms.IBK_BUTTON)))
                .findElement(By.cssSelector("button " + Particles.IBK_FONT)))
                .findElement(By.cssSelector("p")).click();
    }

    public boolean isUnderstandButtonLoanDisplayed() {
        try {
            WebElement restrictionModalElement = getDriver().findElement(By.cssSelector("pl-reentry-logic-modal " + Atoms.IBK_BUTTON));
            return restrictionModalElement.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }
}
