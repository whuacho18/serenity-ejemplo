package pe.interbank.assi.front.ui.modals;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.JavascriptExecutor;

public class ObservedSpouseModalPage extends PageObject {

    public void clickUnderstandButton() throws Exception {
        ((JavascriptExecutor)getDriver()).executeScript("return document.querySelectorAll('pl-observed-spouse-modal ibk-button')[1]" +
                ".shadowRoot.querySelector('button').click()");
    }


}
