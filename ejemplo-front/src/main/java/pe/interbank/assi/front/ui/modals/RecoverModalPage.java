package pe.interbank.assi.front.ui.modals;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pe.interbank.assi.common.utils.Util;
import pe.interbank.assi.front.ui.components.Atoms;
import pe.interbank.assi.front.ui.components.Particles;

/**
 * @author Walter Huacho
 */
public class RecoverModalPage extends PageObject {

    public void clickButtonRecoverCreditCard() throws Exception {
        Util.expandRootElement(Util.expandRootElement(getDriver()
                  .findElement(By.cssSelector("cc-recover-flow-modal div.Modal-body div.grid.mt-40 div.col.col-end " + Atoms.IBK_BUTTON)))
                  .findElement(By.cssSelector("button " + Particles.IBK_FONT)))
                  .findElement(By.cssSelector("p")).click();
    }

    public boolean isButtonRecoverCreditCardDisplayed() {
        try {
            WebElement recoverModalElement = getDriver().findElement(By.cssSelector("cc-recover-flow-modal div.col.col-end " + Atoms.IBK_BUTTON));
            return recoverModalElement.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void clickButtonRecoverLoan() throws Exception {
        Util.expandRootElement(Util.expandRootElement(getDriver()
                .findElement(By.cssSelector("pl-recover-flow-modal div.Modal-body div.grid.mt-40 div.col.col-end " + Atoms.IBK_BUTTON)))
                .findElement(By.cssSelector("button " + Particles.IBK_FONT)))
                .findElement(By.cssSelector("p")).click();
    }

    public boolean isButtonRecoverLoanDisplayed() {
        try {
            WebElement recoverModalElement = getDriver().findElement(By.cssSelector("pl-recover-flow-modal div.col.col-end " + Atoms.IBK_BUTTON));
            return recoverModalElement.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }
}
