# language: es
  Característica: Apertura de cuentas individuales

    Escenario: Realizar apertura de cuenta simple individual de un cliente
      Dado que la persona es un CLIENTE con DNI 13103234
      Cuando se realiza una apertura de cuenta SIMPLE INDIVIDUAL de CLIENTE
      Entonces el cliente debería tener una cuenta creada
      Y el número de expediente debería estar cerrado
      Y el cliente no debería tener ley de protección de datos
      Y el cliente no debería estar afiliado a la banca SMS
