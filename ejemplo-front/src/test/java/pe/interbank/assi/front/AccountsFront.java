package pe.interbank.assi.front;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import net.thucydides.core.annotations.Managed;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/features/IndividualAccount.feature",
        glue = {"pe.interbank.assi"})
public class AccountsFront {
    @Managed
    WebDriver driver;
}
