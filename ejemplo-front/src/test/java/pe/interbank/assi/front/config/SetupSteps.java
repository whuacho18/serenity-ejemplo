package pe.interbank.assi.front.config;

import net.thucydides.core.util.EnvironmentVariables;
import org.junit.Before;

public class SetupSteps {

    private String theRestApiBaseUrl;
    private EnvironmentVariables environmentVariables;

    @Before
    public void configureBaseUrl() {
        theRestApiBaseUrl = environmentVariables.getProperty("restapi.baseurl");
    }
}
